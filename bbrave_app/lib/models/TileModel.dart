import 'package:flutter/cupertino.dart';

class TileModel{

  String imageAssetPath;
  bool isSelected;
  bool matched;
  int matchIndex;

  TileModel({this.imageAssetPath, this.isSelected, this.matchIndex, this.matched});

  void setImageAssetPath(String getImageAssetPath){
    imageAssetPath = getImageAssetPath;
  }

  String getImageAssetPath(){
    return imageAssetPath;
  }

  void setIsSelected(bool getIsSelected){
    isSelected = getIsSelected;
  }

  bool getIsSelected(){
    return isSelected;
  }

  void setIsMatched(bool getIsMatched){
    matched = getIsMatched;
  }

  bool getIsMatched(){
    return matched;
  }

  int getMatchIndex(){
    return matchIndex;
  }

}