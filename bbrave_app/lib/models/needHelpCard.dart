import 'package:flutter/foundation.dart';


class Thumbnail {
  const Thumbnail({
    @required this.id,
    @required this.name,
    @required this.height,
  })  : assert(id != null),
        assert(name != null),
        assert(height != null)
  ;

  final int id;
  final String name;
  final double height;

  String get imageName => 'assets/thumb_images/${id+1}.png';

  @override
  String toString() => "$name (id=$id)";
}

class ThumbnailRepository {
  static List<Thumbnail> loadAllThumbnails() {
    const allThumbnails = <Thumbnail> [
      Thumbnail(
        id: 0,
        name: 'Cyber Bullying',
        height: 3.5,
      ),
      Thumbnail(
        id: 1,
        name: 'Domestic Abuse',
        height: 3.5,
      ),
      Thumbnail(
        id: 2,
        name: 'Disability',
        height: 3.5,
      ),
      Thumbnail(
        id: 3,
        name: 'Kids',
        height: 3,
      ),
      Thumbnail(
        id: 4,
        name: 'Emergency',
        height: 5.4,
      ),
      Thumbnail(
        id: 5,
        name: 'Counselling',
        height: 3.5,
      ),
      Thumbnail(
        id: 6,
        name: 'Law & Crime',
        height: 3.4,
      ),
      Thumbnail(
        id: 7,
        name: 'Mental Health',
        height: 3.3,
      ),
      Thumbnail(
        id: 8,
        name: 'Workplace Bullying',
        height: 2.4,
      ),
    ];

    return allThumbnails;

  }

  static List<Thumbnail> TopThumbnails(){
    const TopThumbs = <Thumbnail>[
      Thumbnail(
        id: 0,
        name: 'Cyber Bullying',
        height: 3.5,
      ),
      Thumbnail(
        id: 1,
        name: 'Domestic Abuse',
        height: 3.5,
      ),
      Thumbnail(
        id: 2,
        name: 'Disability',
        height: 3.5,
      ),
      Thumbnail(
        id: 3,
        name: 'Kids',
        height: 3,
      )
    ];

    return TopThumbs;
  }

  static List<Thumbnail> BottomThumbnails(){
    const BottomThumbs = <Thumbnail>[
      Thumbnail(
        id: 5,
        name: 'Counselling',
        height: 3.5,
      ),
      Thumbnail(
        id: 6,
        name: 'Law & Crime',
        height: 3.4,
      ),
      Thumbnail(
        id: 7,
        name: 'Mental Health',
        height: 3.3,
      ),
      Thumbnail(
        id: 8,
        name: 'Workplace Bullying',
        height: 2.4,
      ),
    ];

    return BottomThumbs;
  }

  static List<Thumbnail> MiddleThumbnails(){
    const MiddleThumbs = <Thumbnail>[
      Thumbnail(
        id: 4,
        name: 'Emergency',
        height: 5.4,
      ),
    ];

    return MiddleThumbs;
  }
}
