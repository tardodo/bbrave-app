import 'package:flutter/material.dart';
import 'constants.dart';
import 'dart:convert';
import 'dart:ui';
import 'package:url_launcher/url_launcher.dart';
import 'expansion_tile.dart';

class ListDetails extends StatelessWidget {
  final bCol = const Color(0xFFF37F49);
  final int id;
  final String name;
  final double height;

  ListDetails(this.id, this.name, this.height);

  List<Contact> parseJson(String response) {
    if (response == "null") {
      return [];
    }
    final parsed =
        json.decode(response.toString()).cast<Map<String, dynamic>>();
    return parsed.map<Contact>((json) => new Contact.fromJson(json)).toList();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
          child: Material(
            child: Container(
              child: new FutureBuilder(
                  future: DefaultAssetBundle.of(context)
                      .loadString('assets/JSON/$id-0.json'),
                  builder: (context, snapshot) {
                    List<Contact> contacts = parseJson(snapshot.data.toString());

                    Widget mySliv;
                    if(contacts.isNotEmpty){
                      mySliv = SliverList(
                        delegate: SliverChildBuilderDelegate(
                              (BuildContext context, int index) {
                            return new Container(//color: Colors.blue,
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
                                  child: new Column(
                                    crossAxisAlignment: CrossAxisAlignment.stretch,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Theme(
                                        data: ThemeData(
                                            accentColor: Color(0xFFef7d40),
                                          textSelectionColor: Color.fromRGBO(251, 189, 83, 0.5),
                                        ),
                                        child: ExpansionTileCard(
                                          borderRadius: BorderRadius.all(Radius.circular(4.0)),
                                          elevation: 10,
                                          title: new Text(
                                            contacts[index].name,
                                            style: new TextStyle(
                                              fontSize: 18.0,
                                              color: Color.fromRGBO(25, 62, 81, 1),
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          children: [
                                            Align(
                                              alignment: Alignment.centerLeft,
                                              child: Padding(
                                                padding: const EdgeInsets.symmetric(
                                                  horizontal: 16.0,
                                                  vertical: 8.0,
                                                ),
                                                child:
                                                new SelectableText(
                                                  contacts[index].body,
                                                  style: new TextStyle(
                                                    fontSize: 16.0,
                                                  ),
                                                ),
                                              ),
                                            ),

                                            button(contacts, index, context),
                                          ]
                                      ),
                                ),
                                    ],
                                    //   ),
                                    //padding: const EdgeInsets.all(15.0),
                                  ),
                                ),
                              );
                          },
                          childCount: contacts == null ? 0 : contacts.length,
                        ),
                      );
                    }

                    return contacts.isNotEmpty
                        ? new ContactList(contact: contacts, id: id, name: name, height: height, mySliver: mySliv,)
                        : new Center(child: new CircularProgressIndicator());
                  }),
            ),
          ),
        );
  }

  Widget button(List<Contact> contacts, int index, BuildContext context){
    if(contacts[index].website == null){
      return ButtonBar(

          alignment: MainAxisAlignment.spaceAround,
          buttonHeight: 52.0,
          buttonMinWidth: 90.0,
          children: <Widget>[
            callButton(context, contacts, index),
          ]
      );
    }else
      return ButtonBar(
        alignment: MainAxisAlignment.spaceAround,
        buttonHeight: 52.0,
        buttonMinWidth: 90.0,
        children: <Widget>[
          callButton(context, contacts, index),
          FlatButton(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(4.0)),
            onPressed: () async{
              var url = contacts[index].website;
              if (await canLaunch(url)) {
                await launch(url);
              } else {
                throw 'Could not launch $url';
              }
            },
            child: Column(
              children: <Widget>[
                Icon(Icons.public, color: Color(0xFFef7d40),),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 2.0),
                ),
                Text('Website', style: TextStyle(color: Color(0xFFef7d40)),),
              ],
            ),
          ),
        ]
    );
  }

  Widget callButton(BuildContext context,List<Contact> contacts, int index) {
    if (contacts[index].otherNums == null) {
      return FlatButton(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(4.0)),
        onPressed: () async {
          var uri = contacts[index].phone;
          if (await canLaunch("tel:$uri")) {
            await launch("tel:$uri");
          } else {
            throw 'Could not launch $uri';
          }
        },
        child: Column(
          children: <Widget>[
            Icon(Icons.phone, color: Color(0xFFef7d40),),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 2.0),
            ),
            Text('Call', style: TextStyle(color: Color(0xFFef7d40)),),
          ],
        ),
      );
    }else {
      return FlatButton(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(4.0)),
        onPressed: () => callPopUp(context, contacts, index)
        ,
        child: Column(
          children: <Widget>[
            Icon(Icons.phone, color: Color(0xFFef7d40),),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 2.0),
            ),
            Text('Call', style: TextStyle(color: Color(0xFFef7d40)),),
          ],
        ),
      );
    }
  }

  void callPopUp(BuildContext context, List<Contact> contacts, int ind) {
    List<String> nums = contacts[ind].otherNums.split(",");
    showModalBottomSheet( context: context,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
                top: Radius.circular(10.0))),
        builder: (context) {
          return ListView.builder(
              itemCount: nums.length,
              shrinkWrap: true,
              itemBuilder: (BuildContext cont, int index2) {
                return ListTile(
                  contentPadding: EdgeInsets.all(8.0),
                  leading: Icon(Icons.phone,
                    color: Color(0xFFef7d40),
                  ),
                  title: Text(
                    nums[index2],
                    style: TextStyle(
                        fontSize: 16
                    ),
                  ),
                  onTap: () async {
                    var uri = nums[index2];
                    if (await canLaunch("tel:$uri")) {
                      await launch("tel:$uri");
                      Navigator.pop(cont);
                    } else {
                      throw 'Could not launch $uri';
                    }
                  },
                );
              });
        });
  }
}

class Contact {
  final String name;
  final String body;
  final String website;
  final String phone;
  final String otherNums;

  Contact({this.name, this.body, this.website, this.phone, this.otherNums});

  factory Contact.fromJson(Map<String, dynamic> json) {
    return new Contact(
      name: json['name'] as String,
      body: json['body'] as String,
      website: json['link_url'] as String,
      phone: json['phone'] as String,
      otherNums: json['other_phones'] as String,
    );
  }
}

class ContactList extends StatefulWidget {
  final List<Contact> contact;
  final int id;
  final String name;
  final double height;
  final Widget mySliver;

  ContactList({Key key, this.contact, this.id, this.name, this.height, this.mySliver})
      : super(key: key);

  @override
  _ContactListState createState() => _ContactListState();
}

class _ContactListState extends State<ContactList> with AutomaticKeepAliveClientMixin {

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    double screenHeight = MediaQuery.of(context).size.height;
    super.build(context);
    return Container(
        child: CustomScrollView(
          physics: const AlwaysScrollableScrollPhysics(),
            slivers: [
              SliverAppBar(
                elevation: 10,
                expandedHeight: screenHeight/widget.height,
                pinned: true,
                flexibleSpace: //_cardInfo,

                Center(child:
                Container(
                  child: Stack(
                      children:[
                    Positioned.fill(
                        child: Image.asset("assets/thumb_images/${widget.id+1}.png", fit: BoxFit.fitWidth,)
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(15, 5, 15, 5),
                      child: Card(
                        shape: RoundedRectangleBorder(
                            side: BorderSide(
                                color: Color.fromRGBO(25, 62, 81, 1),
                                width: 1.0
                            ),
                            borderRadius: BorderRadius.circular(4.0)
                        ),
                        elevation: 10,
                        child: Center(
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(20, 40, 20, 0),
                            child: Text(
                              tBody[widget.id],
                              style: TextStyle(
                                  fontSize: 14 ,
                                  color: Colors.black
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ]
                  ),
                ),
                ),

                title:Text(
                  widget.name,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontSize: 25,
                      color: Color.fromRGBO(25, 62, 81, 1),
                      fontWeight: FontWeight.bold
                  ),
                ),
                centerTitle: true,
                leading: IconButton(
                  icon: Icon(
                      Icons.arrow_back,
                      color: Color(0xFFef7d40)
                  ),
                  onPressed: (){
                    Navigator.of(context).pop(null);
                    },
                  iconSize: 30,
                  padding: EdgeInsets.only(left: 20),
                ),
              ),

              widget.mySliver,
            ]
        )
    );
  }
}

