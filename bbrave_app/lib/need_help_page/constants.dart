import 'package:flutter/cupertino.dart';

final String cyber =
    "Cyber Bullying, also known as online bullying, takes place virtually. This form of bullying uses technology to harass and target another person but the consequences are just as real and harmful as those resulting from conventional bullying.";
final String dom =
    "Domestic abuse involves abuse by one person against another in a domestic setting including families and intimate relationships. The abuse can be physical, emotional and/or psychological  – and can affect anyone. Here’s where you can turn to for help:";
final String dis =
    "Disability abuse involves exploitation and abuse of a person with a disability. This form of abuse can be physical, psychological, emotional and/or financial. Here are links and contacts you can turn to for help and support:";
final String edu =
    "Bullying takes place in several settings. At school, it can have devastating effects on a child’s confidence and self-esteem. Bullying can be also experienced outside of school e.g. during football practice or dancing lessons. If you’re looking for support and advice, here’s a list of contacts who can help you out.";
final String emergency =
    "Here’s a list of Emergency Contacts in Malta you need to know:";
final String couns =
    "Counselling is a talking therapy that takes place in a safe, supportive and confidential environment. It provides individuals with the opportunity to discuss emotions with a trained therapist freely without fear of judgement or criticism.";
final String law =
    "Certain bullying behaviour can amount to a criminal offence. If this is the case, or if you would like to ask for advice, the following contacts may help you seek protection and ascertain your rights:";
final String mental =
    "Individuals who suffer from bullying are more likely to experience mental health issues, whether in the short term or later in life. Common consequences are social problems, anxiety and depression, among others. The following are some contacts you can refer to if you are seeking help:";
final String work =
    "Bullying at work takes on many forms. It may involve repeated patterns of behaviours such as rudeness, insulting behaviour, abuse or misuse of power and humiliation. However, it can also be the subtle act of exclusion and ostracism.\n\n\n"
    "If you’re experiencing bullying at work, here are some contacts which can provide you with the necessary support.";

final List<String> tBody = [
  cyber,
  dom,
  dis,
  edu,
  emergency,
  couns,
  law,
  mental,
  work
];
