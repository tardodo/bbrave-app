import 'package:bbrave_app/need_help_page/constants.dart';
import 'package:flutter/material.dart';
import 'package:bbrave_app/models/needHelpCard.dart';
import 'list_details_page.dart';
import 'package:google_fonts/google_fonts.dart';

class PlaceholderWidget extends StatelessWidget {
  final Color color;

  PlaceholderWidget(this.color);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: color,
    );
  }
}

class CardView extends StatelessWidget {
  List<GestureDetector> _buildGridCards(BuildContext context, int section) {
    List<Thumbnail> thumbnails;
    if (section == 1) {
      thumbnails = ThumbnailRepository.TopThumbnails();
      if (thumbnails == null || thumbnails.isEmpty) {
        return const <GestureDetector>[];
      }
    } else if (section == 2) {
      thumbnails = ThumbnailRepository.MiddleThumbnails();
      if (thumbnails == null || thumbnails.isEmpty) {
        return const <GestureDetector>[];
      }
    } else if (section == 3) {
      thumbnails = ThumbnailRepository.BottomThumbnails();
      if (thumbnails == null || thumbnails.isEmpty) {
        return const <GestureDetector>[];
      }
    } else if (section == 4) {
      thumbnails = ThumbnailRepository.loadAllThumbnails();
      if (thumbnails == null || thumbnails.isEmpty) {
        return const <GestureDetector>[];
      }
    } else {
      if (thumbnails == null || thumbnails.isEmpty) {
        return const <GestureDetector>[];
      }
    }

    if (section == 2) {
      return thumbnails.map((thumbnail) {
        return GestureDetector(
          onTap: () {
            print("Pressed ${thumbnail.id}");
            Route route = MaterialPageRoute(
                builder: (context) => ListDetails(
                    thumbnail.id, thumbnail.name, thumbnail.height));
            Navigator.push(context, route);
          },
          child: Card(
              elevation: 10,
              clipBehavior: Clip.antiAlias,
              child: Image.asset(thumbnail.imageName,fit: BoxFit.cover)),
        );
      }).toList();
    } else {
      return thumbnails.map((thumbnail) {
        return GestureDetector(
          onTap: () {
            print("Pressed ${thumbnail.id}");
            Route route = MaterialPageRoute(
                builder: (context) => ListDetails(
                    thumbnail.id, thumbnail.name, thumbnail.height));
            Navigator.push(context, route);
          },
          child: Card(
            elevation: 10,
            clipBehavior: Clip.antiAlias,
            child:
                AspectRatio(
                  aspectRatio: 10.0/9.0,
                  child: Image.asset(thumbnail.imageName, fit: BoxFit.fill),
                ),
          ),
        );
      }).toList();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      //color: Color.fromRGBO(251, 186, 92, 1),
      child: CustomScrollView(
          physics: const AlwaysScrollableScrollPhysics(),
          slivers: [
            SliverPadding(padding: EdgeInsets.fromLTRB(32, 22, 32, 5),
              sliver: SliverList(delegate: SliverChildListDelegate(
                [Center(child:
                Text("I NEED HELP",
                  style: GoogleFonts.robotoSlab(
                    fontSize: 25,
                    fontStyle: FontStyle.normal,
                    color: Color.fromRGBO(37, 61, 76, 1),
                    fontWeight: FontWeight.bold
                  ),
                ),
                )
                ],
              ),
              ),
            ),
            SliverPadding(
              padding: EdgeInsets.all(10),
              sliver: SliverGrid.count(
                  mainAxisSpacing: 10,
                  crossAxisSpacing: 10,
                  crossAxisCount: 2,
                  childAspectRatio: 10.0 / 9,
                  children: _buildGridCards(context, 1)),
            ),
            SliverPadding(
              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              sliver: SliverGrid.count(
                  crossAxisCount: 1,
                  childAspectRatio: 10.0 / 4,
                  children: _buildGridCards(context, 2)),
            ),
            SliverPadding(
              padding: EdgeInsets.all(10),
              sliver: SliverGrid.count(
                  mainAxisSpacing: 10,
                  crossAxisSpacing: 10,
                  crossAxisCount: 2,
                  childAspectRatio: 10.0 / 9,
                  children: _buildGridCards(context, 3)),
            )
          ]),
    );
  }
}
