import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'game.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GameFront extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        body: ListView(
          children: [
            titleSection,
            text,
            GameWidget(),
          ],
        ),
      ),
    );
  }
}

Widget titleSection = Container(
  padding: const EdgeInsets.all(32),
  child: Text(
    'PLAY',
    textAlign: TextAlign.center,
    style: GoogleFonts.pressStart2p(
      fontSize: 64,
      fontStyle: FontStyle.normal,
    ),
  ),
);

Widget text = Container(
  padding: const EdgeInsets.all(32),
  child: Text(
    'Choose your level:',
    softWrap: true,
    textAlign: TextAlign.center,
    style: GoogleFonts.robotoSlab(
      fontSize: 20,
      fontWeight: FontWeight.bold,
      color: Colors.black,
    ),
  ),
);

class GameWidget extends StatefulWidget {
  GameWidget({Key key}) : super(key: key);

  @override
  _GameWidgetState createState() => _GameWidgetState();
}

class _GameWidgetState extends State<GameWidget> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  String easyTime;
  String mediumTime;
  String hardTime;

  Future<String> _getBest(int difficulty) async {
    final SharedPreferences prefs = await _prefs;
    final String bestTime = prefs.getString('best $difficulty') ?? "00:00";

    return bestTime;
  }

  Future<bool> _remove(int difficulty) async {
    final SharedPreferences prefs = await _prefs;
    return prefs.remove('best $difficulty') ?? false;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        ButtonTheme(
          minWidth: 180.0,
          height: 50.0,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(4))),
          child: RaisedButton(
            color: Colors.green,
            onPressed: () {
              Route route =
                  MaterialPageRoute(builder: (context) => Game(difficulty: 1));
              Navigator.push(context, route);
            },
            child: Text(
              "Easy",
              style: TextStyle(fontSize: 18),
            ),
          ),
        ),
        Container(
          height: 8.0,
        ),
        ButtonTheme(
          minWidth: 180.0,
          height: 50.0,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(4))),
          child: RaisedButton(
            color: Colors.yellow,
            onPressed: () {
              Route route =
                  MaterialPageRoute(builder: (context) => Game(difficulty: 2));
              Navigator.push(context, route);
            },
            child: Text(
              "Medium",
              style: TextStyle(fontSize: 18),
            ),
          ),
        ),
        Container(
          height: 8.0,
        ),
        ButtonTheme(
          minWidth: 180.0,
          height: 50.0,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(4))),
          child: RaisedButton(
            color: Colors.red,
            onPressed: () {
              Route route =
                  MaterialPageRoute(builder: (context) => Game(difficulty: 3));
              Navigator.push(context, route);
            },
            child: Text(
              "Hard",
              style: TextStyle(fontSize: 18),
            ),
          ),
        ),
        SizedBox(height: 40,),
        ButtonTheme(
          minWidth: 180.0,
          height: 50.0,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(4))),
          child: RaisedButton(
            color: Color(0xFFef7d40),
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return StatefulBuilder(builder: (context, setState) {
                      return AlertDialog(
                        title: Text("Top Scores", style: TextStyle(fontSize: 28, fontStyle: FontStyle.italic),),
                        content:
                            Column(mainAxisSize: MainAxisSize.min, children: [
                              Align(alignment: Alignment.centerRight,
                                child:
                              Text("Reset", textAlign: TextAlign.end,),),
                          Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Center(
                                    child: FutureBuilder<String>(
                                        future: _getBest(1),
                                        builder: (BuildContext context,
                                            AsyncSnapshot<String> snapshot) {
                                          switch (snapshot.connectionState) {
                                            case ConnectionState.waiting:
                                              return const CircularProgressIndicator();
                                            default:
                                              if (snapshot.hasError) {
                                                return Text(
                                                    'Error: ${snapshot.error}');
                                              } else if (snapshot.hasData ==
                                                  true) {
                                                easyTime = snapshot.data;
                                                return RichText(
                                                    text: TextSpan(
                                                        text:"Easy \n",
                                                        style: TextStyle(
                                                            fontSize: 25,
                                                            fontWeight: FontWeight.bold,
                                                          color: Colors.black
                                                        ),
                                                        children:[
                                                          TextSpan(
                                                            text: "\t\t\t $easyTime",
                                                            style: TextStyle(
                                                                fontSize: 22,
                                                                fontWeight: FontWeight.normal,


                                                            ),
                                                          )
                                                        ],
                                                    ));
                                              } else
                                                return Text("Nothing bruh");
                                          }
                                        })),
                                IconButton(
                                    icon: Icon(Icons.refresh),
                                    iconSize: 30,
                                    onPressed: () async {
                                      if(easyTime != "00:00") {
                                        AlertDialog(content: Text("data"),);
                                        await _remove(1);
                                        setState(() {});
                                      }
                                    })
                              ]),
                          Divider(thickness: 2,),
                          Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Center(
                                    child: FutureBuilder<String>(
                                        future: _getBest(2),
                                        builder: (BuildContext context,
                                            AsyncSnapshot<String> snapshot) {
                                          switch (snapshot.connectionState) {
                                            case ConnectionState.waiting:
                                              return const CircularProgressIndicator();
                                            default:
                                              if (snapshot.hasError) {
                                                return Text(
                                                    'Error: ${snapshot.error}');
                                              } else if (snapshot.hasData ==
                                                  true) {
                                                mediumTime = snapshot.data;
                                                return RichText(
                                                    text: TextSpan(
                                                      text:"Medium \n",
                                                      style: TextStyle(
                                                          fontSize: 25,
                                                          fontWeight: FontWeight.bold,
                                                          color: Colors.black
                                                      ),
                                                      children:[
                                                        TextSpan(
                                                          text: "\t\t\t $mediumTime",
                                                          style: TextStyle(
                                                            fontSize: 22,
                                                            fontWeight: FontWeight.normal,


                                                          ),
                                                        )
                                                      ],
                                                    ));
                                              } else
                                                return Text("Nothing bruh");
                                          }
                                        })),
                                IconButton(
                                    icon: Icon(Icons.refresh),
                                    iconSize: 30,
                                    onPressed: () async {
                                      if(mediumTime != "00:00") {
                                        await _remove(2);
                                        setState(() {});
                                      }
                                    })
                              ]),
                              Divider(thickness: 2,),
                          Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Center(
                                    child: FutureBuilder<String>(
                                        future: _getBest(3),
                                        builder: (BuildContext context,
                                            AsyncSnapshot<String> snapshot) {
                                          switch (snapshot.connectionState) {
                                            case ConnectionState.waiting:
                                              return const CircularProgressIndicator();
                                            default:
                                              if (snapshot.hasError) {
                                                return Text(
                                                    'Error: ${snapshot.error}');
                                              } else if (snapshot.hasData ==
                                                  true) {
                                                hardTime = snapshot.data;
                                                return RichText(
                                                    text: TextSpan(
                                                      text:"Hard \n",
                                                      style: TextStyle(
                                                          fontSize: 25,
                                                          fontWeight: FontWeight.bold,
                                                          color: Colors.black
                                                      ),
                                                      children:[
                                                        TextSpan(
                                                          text: "\t\t\t $hardTime",
                                                          style: TextStyle(
                                                            fontSize: 22,
                                                            fontWeight: FontWeight.normal,


                                                          ),
                                                        )
                                                      ],
                                                    ));
                                              } else
                                                return Text("Nothing bruh");
                                          }
                                        })),
                                IconButton(
                                    icon: Icon(Icons.refresh),
                                    iconSize: 30,
                                    onPressed: () async {
                                      if(hardTime != "00:00") {
                                        await _remove(3);
                                        setState(() {});
                                      }
                                    })
                              ]),
                        ]),
                      );
                    });
                  });
            },
            child: Text(
              "Top Scores",
              style: TextStyle(fontSize: 18),
            ),
          ),
        ),
      ],
    );
  }
}
