import 'package:bbrave_app/models/TileModel.dart';
import 'package:flutter/cupertino.dart';

String selectedTile = "";
int matchIndex;
int selectedIndex ;
bool selected = true;
int points = 0;
const int totalEasy = 6;
const int totalNorm = 8;
const int totalHard = 10;

List<TileModel> myPairs = new List<TileModel>();
List<bool> clicked = new List<bool>();

List<String> easyNames = ["boy","girl", "teacher","childrenHoldingHands","muscles","ear"];
List<String> normNames = ["boy","girl","teacher","childrenHoldingHands","childrenPlaying","childrenFightingToy","muscles","ear"];
List<String> hardNames = ["childrenPlaying","childrenFightingToy","angryFace","crying","childrenHoldingHands","muscles","ear","smileyFace","smilingFace","sadFace"];


List<TileModel> getEasyPairs(){
  List<TileModel> pairs = new List<TileModel>();

  TileModel tileModel = new TileModel();

  for(int i = 0;i<6;i++){
    tileModel.setImageAssetPath("assets/game_images/" + easyNames[i] +".png");
    tileModel.setIsSelected(false);
    tileModel.setIsMatched(false);
    tileModel.matchIndex = i;
    pairs.add(tileModel);
    tileModel = new TileModel();
    tileModel.setImageAssetPath("assets/game_images/" + easyNames[i] +".png");
    tileModel.setIsSelected(false);
    tileModel.setIsMatched(false);
    tileModel.matchIndex = i;
    pairs.add(tileModel);
    tileModel = new TileModel();
  }

  return pairs;
}

List<TileModel> getNormPairs(){
  List<TileModel> pairs = new List<TileModel>();

  TileModel tileModel = new TileModel();

  for(int i = 0;i<8;i++){
    tileModel.setImageAssetPath("assets/game_images/" + normNames[i] +".png");
    tileModel.setIsSelected(false);
    tileModel.setIsMatched(false);
    tileModel.matchIndex = i;
    pairs.add(tileModel);
    tileModel = new TileModel();
    tileModel.setImageAssetPath("assets/game_images/" + normNames[i] +".png");
    tileModel.setIsSelected(false);
    tileModel.setIsMatched(false);
    tileModel.matchIndex = i;
    pairs.add(tileModel);
    tileModel = new TileModel();
  }

  return pairs;
}

List<TileModel>  getHardPairs(){

  List<TileModel> pairs = new List<TileModel>();

  TileModel tileModel = new TileModel();

  for(int i = 0;i<10;i++){
    tileModel.setImageAssetPath("assets/game_images/" + hardNames[i] +".png");
    tileModel.setIsSelected(false);
    tileModel.setIsMatched(false);
    tileModel.matchIndex = i;
    pairs.add(tileModel);
    tileModel = new TileModel();
    tileModel.setImageAssetPath("assets/game_images/" + hardNames[i] +".png");
    tileModel.setIsSelected(false);
    tileModel.setIsMatched(false);
    tileModel.matchIndex = i;
    pairs.add(tileModel);
    tileModel = new TileModel();
  }

  return pairs;
}

List<TileModel>  getQuestionPairs(int difficulty){
  int totalPairs;

  if(difficulty == 1){
    totalPairs = 6;
  }else if(difficulty == 2){
    totalPairs = 8;
  }else if(difficulty == 3){
    totalPairs = 10;
  }

  List<TileModel> pairs = new List<TileModel>();
  TileModel tileModel = new TileModel();

  for(int i = 0; i<totalPairs; i++){
    tileModel.setImageAssetPath("assets/signature_logo-01.png");
    tileModel.setIsSelected(false);
    pairs.add(tileModel);
    pairs.add(tileModel);
    tileModel = new TileModel();
  }

  return pairs;
}