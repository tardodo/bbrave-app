import 'dart:async';

import 'package:flutter/material.dart';
import 'data.dart';
import 'package:bbrave_app/models/TileModel.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:audioplayers/audio_cache.dart';

bool done = false;
bool ready = false;
var timer = Stopwatch();
Timer time;

class Game extends StatefulWidget {
  final int difficulty;
  Game({this.difficulty});
  @override
  _GameState createState() => _GameState(difficulty: difficulty);
}

class _GameState extends State<Game> {
   int difficulty;
  _GameState({this.difficulty});

  List<TileModel> gridViewTiles = new List<TileModel>();
  List<TileModel> questionPairs = new List<TileModel>();
  int totalPoints;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  final dur = const Duration(seconds: 1);
  String timeDisplay = "00:00";

  Future<String> _getBest() async {
    final SharedPreferences prefs = await _prefs;
    final String bestTime = prefs.getString('best $difficulty') ?? timeDisplay;

    return bestTime;
  }

  Future<bool> setBest() async {
    final SharedPreferences prefs = await _prefs;
    final success = prefs.setString('best $difficulty', timeDisplay);

    return success;
  }

  void setTimer() {
    time = Timer(dur, whileRunning);
  }

  void startTimer() {
    timer.start();
    setTimer();
    print("start");
  }

  void whileRunning() {
    if (timer.isRunning) {
      setTimer();
    }
    setState(() {
      timeDisplay = (timer.elapsed.inMinutes % 60).toString().padLeft(2, "0") +
          ":" +
          (timer.elapsed.inSeconds % 60).toString().padLeft(2, "0");
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (difficulty == 1) {
      totalPoints = totalEasy;
    } else if (difficulty == 2) {
      totalPoints = totalNorm;
    } else if (difficulty == 3) {
      totalPoints = totalHard;
    }
    reStart();
  }

  void reStart() {
    if (difficulty == 1) {
      myPairs = getEasyPairs();
      totalPoints = totalEasy;
    } else if (difficulty == 2) {
      myPairs = getNormPairs();
      totalPoints = totalNorm;
    } else if (difficulty == 3) {
      myPairs = getHardPairs();
      totalPoints = totalHard;
    }

    timeDisplay = "00:00";
    points = 0;
    done = false;
    ready = false;
    selectedTile = "";

    myPairs.shuffle();

    gridViewTiles = myPairs;
    if (difficulty == 1) {
      Future.delayed(const Duration(seconds: 2), () {
        if (this.mounted) {
          setState(() {
            print("2 seconds done");
            // Here you can write your code for open new view
            questionPairs = getQuestionPairs(difficulty);
            gridViewTiles = questionPairs;
            selected = false;
            ready = true;
          });
          timer.reset();
          time?.cancel();
          startTimer();
        }
      });
    } else if (difficulty == 2) {
      Future.delayed(const Duration(seconds: 1), () {
// Here you can write your code
        if (this.mounted) {
          setState(() {
            print("2 seconds done");
            // Here you can write your code for open new view
            questionPairs = getQuestionPairs(difficulty);
            gridViewTiles = questionPairs;
            selected = false;
            ready = true;
          });
          timer.reset();
          time?.cancel();
          startTimer();
        }
      });
    } else if (difficulty == 3) {
      setState(() {
        print("2 seconds done");
        // Here you can write your code for open new view
        questionPairs = getQuestionPairs(difficulty);
        gridViewTiles = questionPairs;
        selected = false;
        ready = true;
      });
      timer.reset();
      time?.cancel();
      startTimer();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          done == false
              ? Center(
                  child: Text(
                  "$timeDisplay  ",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 40,
                    fontWeight: FontWeight.bold,
                  ),
                ))
              : Container()
        ],
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.white,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Image.asset(
              'assets/bbrave_logo.jpg',
              fit: BoxFit.cover,
              height: 35.0,
            ),
          ],
        ),
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 20,
              ),
              done == false
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "$points/$totalPoints",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.w500),
                        ),
                        Text(
                          "Pairs Found",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.w300),
                        ),
                      ],
                    )
                  : Container(),
              // SizedBox(
              //   height: 40,
              // ),
              done == false
                  ? GridView(
                      shrinkWrap: true,
                      padding: difficulty == 1
                          ? EdgeInsets.only(left: 35, right: 35, top: 20)
                          : EdgeInsets.only(top: 20),
                      //physics: ClampingScrollPhysics(),
                      scrollDirection: Axis.vertical,
                      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                          crossAxisSpacing: 2 * (3 / difficulty),
                          mainAxisSpacing: 5.0,
                          maxCrossAxisExtent: 100.0),
                      children: List.generate(gridViewTiles.length, (index) {
                        return Tile(
                          totalpoints: totalPoints,
                          imagePathUrl:
                              gridViewTiles[index].getImageAssetPath(),
                          tileIndex: index,
                          parent: this,
                        );
                        // );
                      }),
                    )
                  : Container(
                      child: Column(
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              points = 0;
                              reStart();
                            });
                          },
                          child: Container(
                            height: 50,
                            width: 200,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              color: Color.fromRGBO(239, 125, 64, 1),
                              borderRadius: BorderRadius.circular(4),
                            ),
                            child: Text(
                              "Replay",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 17,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        difficulty <3?  GestureDetector(
                          onTap: () {
                            setState(() {
                              difficulty = difficulty+1;
                              reStart();
                            });
                          },
                          child: Container(
                            height: 50,
                            width: 200,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              color: Color.fromRGBO(239, 125, 64, 1),
                              borderRadius: BorderRadius.circular(4),
                            ),
                            child: Text(
                              "Next Level",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 17,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        ):Container(),
                        SizedBox(
                          height: difficulty<3? 20:0,
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Container(
                            height: 50,
                            width: 200,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              border: Border.all(
                                  color: Color.fromRGBO(239, 125, 64, 1),
                                  width: 2),
                              borderRadius: BorderRadius.circular(4),
                            ),
                            child: Text(
                              "Home",
                              style: TextStyle(
                                  color: Color.fromRGBO(239, 125, 64, 1),
                                  fontSize: 17,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 40,
                        ),
                        Center(
                            child: Text("You beat it in $timeDisplay!",
                                style: TextStyle(fontSize: 25))),
                        SizedBox(
                          height: 40,
                        ),
                        Center(
                            child: FutureBuilder<String>(
                                future: _getBest(),
                                builder: (BuildContext context,
                                    AsyncSnapshot<String> snapshot) {
                                  switch (snapshot.connectionState) {
                                    case ConnectionState.waiting:
                                      return const CircularProgressIndicator();
                                    default:
                                      if (snapshot.hasError) {
                                        return Text('Error: ${snapshot.error}');
                                      } else if (snapshot.hasData == true) {
                                        if (timeDisplay
                                                .compareTo(snapshot.data) <=
                                            0) {
                                          setBest();
                                          return Text(
                                            "New Best Time: $timeDisplay",
                                            style: TextStyle(fontSize: 25),
                                          );
                                        } else
                                          return Text(
                                              "Best Time: ${snapshot.data}",
                                              style: TextStyle(fontSize: 25));
                                      } else
                                        return Text("Nothing bruh");
                                  }
                                })),
                      ],
                    ))
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    time?.cancel();
    super.dispose();
  }
}

class Tile extends StatefulWidget {
  String imagePathUrl;
  int tileIndex;
  int matchIndex;
  int totalpoints;
  _GameState parent;

  Tile({this.imagePathUrl, this.tileIndex, this.parent, this.totalpoints});

  @override
  _TileState createState() => _TileState();
}

class _TileState extends State<Tile> {

  void playSoundCorrect() {
    AudioCache cache = new AudioCache();
    cache.play("131660__bertrof__game-sound-correct.wav");
  }

  void playSoundWrong() {
    AudioCache cache = new AudioCache();
    cache.play("131657__bertrof__game-sound-wrong.wav");
  }

  void playSoundTap() {
    AudioCache cache = new AudioCache();
    cache.play("131658__bertrof__game-sound-selection.wav");
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: ready
            ? () {
                if (!myPairs[widget.tileIndex].getIsSelected() &&
                    myPairs[widget.tileIndex].getIsMatched() == false &&
                    !selected) {
                  //this.widget.parent.setState(() {});
                  setState(() {
                    myPairs[widget.tileIndex].setIsSelected(true);
                    print("boom");
                  });
                  if (selectedTile != "" &&
                      myPairs[widget.tileIndex].getIsMatched() == false &&
                      selectedIndex != widget.tileIndex) {
                    /// testing if the selected tiles are same
                    //  if (selectedTile == myPairs[widget.tileIndex].getImageAssetPath() && selectedIndex != widget.tileIndex) {
                    if (matchIndex ==
                            myPairs[widget.tileIndex].getMatchIndex() &&
                        selectedIndex != widget.tileIndex) {
                      print("add point");
                      points = points + 1;
                      playSoundCorrect();
                      if (points == widget.totalpoints) {
                        if (timer.isRunning) {
                          timer.stop();
                        }
                        //String besttime = widget.parent._getBest().toString();
                        //if(widget.parent.timeDisplay.compareTo(besttime) < 0){
                        // widget.parent.setBest();
                        //}
                        //widget.parent.setBest();
                        //timer.stop();
                        Future.delayed(const Duration(seconds: 1), () {
                          this.widget.parent.setState(() {
                            done = true;
                          });
                        });
                      }
                      print(selectedTile + " thishis" + widget.imagePathUrl);
                      myPairs[widget.tileIndex].setIsMatched(true);
                      myPairs[selectedIndex].setIsMatched(true);
                      //TileModel tileModel = new TileModel();
                      print(widget.tileIndex);
                      selected = true;
                      // Future.delayed(const Duration(seconds: 2), () {
                      //tileModel.setImageAssetPath("");
                      //myPairs[widget.tileIndex] = tileModel;
                      print(selectedIndex);
                      //myPairs[selectedIndex] = tileModel;
                      this.widget.parent.setState(() {});
                      setState(() {
                        selected = false;
                      });
                      selectedTile = "";
                      //  });
                    } else {
                      print(selectedTile +
                          " thishis " +
                          myPairs[widget.tileIndex].getImageAssetPath());
                      print("wrong choice");
                      print(widget.tileIndex);
                      print(selectedIndex);
                      playSoundWrong();
                      selected = true;
                      Future.delayed(const Duration(seconds: 1), () {
                        this.widget.parent.setState(() {
                          myPairs[widget.tileIndex].setIsSelected(false);
                          myPairs[selectedIndex].setIsSelected(false);
                        });
                        setState(() {
                          selected = false;
                        });
                      });

                      selectedTile = "";
                    }
                  } else {
                    playSoundTap();
                    setState(() {
                      selectedTile =
                          myPairs[widget.tileIndex].getImageAssetPath();
                      selectedIndex = widget.tileIndex;
                      matchIndex = myPairs[widget.tileIndex].getMatchIndex();
                    });

                    print(selectedTile);
                    print(selectedIndex);
                  }
                }
              }
            : () {},
        child: Card(
          color: ready
              ? myPairs[widget.tileIndex].getIsSelected() == false
                  ? Color.fromRGBO(239, 125, 64, 1)
                  : Colors.white
              : Colors.white,
          shape: RoundedRectangleBorder(
              side: new BorderSide(
                  color: myPairs[widget.tileIndex].getIsMatched() == false
                      ? myPairs[widget.tileIndex].getIsSelected()
                          ? Color.fromRGBO(239, 125, 64, 1)
                          : Color.fromRGBO(37, 61, 76, 1)
                      : Color.fromRGBO(37, 61, 76, 1),
                  width: 2.0),
              borderRadius: BorderRadius.all(Radius.circular(4.0))),
          child: Container(
              //color: Colors.black,
              margin: EdgeInsets.all(5),
              child: Image.asset(myPairs[widget.tileIndex].getIsSelected()
                  ? myPairs[widget.tileIndex].getImageAssetPath()
                  : widget.imagePathUrl)
              /* child: ready ? myPairs[widget.tileIndex].getIsSelected()?
                 Center(child:
             Text("${myPairs[widget.tileIndex].getMatchIndex()}", style: TextStyle(fontSize: 40),)):
             Image.asset(widget.imagePathUrl): Center(child:
             Text("${myPairs[widget.tileIndex].getMatchIndex()}", style: TextStyle(fontSize: 40),)), */
              ),
        ));
    /* Container(
        margin: EdgeInsets.all(5),
        child: myPairs[widget.tileIndex].getImageAssetPath() != ""
            ? Image.asset(myPairs[widget.tileIndex].getIsSelected()
                ? myPairs[widget.tileIndex].getImageAssetPath()
                : widget.imagePathUrl)
            : Container(
                color: Colors.white,
                child: Image.asset("assets/correct.png"),
              ),
      ), */
  }
}
