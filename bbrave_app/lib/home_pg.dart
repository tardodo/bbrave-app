import 'package:bbrave_app/game_page/game_front.dart';
import 'package:flutter/material.dart';
import 'need_help_page/need_help.dart';
import 'package:bbrave_app/custom_icon/need_help_icon_icons.dart';
import 'webview.dart';
import 'join_page/enroll.dart';
import 'ContactUs.dart';

class Home extends StatefulWidget {
  final int index;
  Home({this.index});
  @override
  State<StatefulWidget> createState() {
    return _HomeState();
  }
}

class _HomeState extends State<Home> {
  //final bCol = const Color(0xFFF37F49);
  final bCol = const Color(0xFFef7d40);
  int _currentIndex = 0;
  bool showBar = true;
  final List<Widget> _children = [
    WebViewContainer(key: UniqueKey(),url: "https://bbrave.org.mt/news/", index: 0,),
    Enroll(),
    CardView(),
    GameFront(),
    Contact()
  ];

  @override
  void initState(){
    super.initState();
    if(widget.index != null){
      setState(() {
        _currentIndex = widget.index;
        if(_currentIndex == 0){
          showBar = false;
        }
      });
    }else{
      _currentIndex = 2;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: showBar ? AppBar(
        backgroundColor: Colors.white,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Image.asset(
              'assets/bbrave_logo.jpg',
              fit: BoxFit.cover,
              height: 35.0,
            ),
          ],
        ),
      ): null,
      body: _children[_currentIndex], // new
      bottomNavigationBar: BottomNavigationBar(
        //type: BottomNavigationBarType.fixed,
        onTap: onTabTapped, // new
        currentIndex: _currentIndex, // new
        //backgroundColor: bCol,
        selectedLabelStyle: TextStyle(fontWeight: FontWeight.bold,  ),
        selectedIconTheme: IconThemeData(size: 30),
        selectedItemColor: Color.fromRGBO(37, 61, 76, 1),
        unselectedItemColor: Color.fromRGBO(37, 61, 76, 0.4),
        selectedFontSize: 15,
        items: [
          new BottomNavigationBarItem(
            icon: Icon(Icons.library_books),
            title: Text('Articles'),
            backgroundColor: bCol,
          ),
          new BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text('Join Us'),
            backgroundColor: bCol,
          ),
          new BottomNavigationBarItem(
            icon: Icon(NeedHelpIcon.needhelp),
            title: Text('I Need Help'),
            backgroundColor: bCol,
          ),
          new BottomNavigationBarItem(
            icon: Icon(Icons.videogame_asset),
            title: Text('Game'),
            backgroundColor: bCol,
          ),
          new BottomNavigationBarItem(
            icon: Icon(Icons.mail),
            title: Text('Contact Us'),
            backgroundColor: bCol,
          ),
        ],
      ),
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
    if(index == 0){
      setState(() {
        showBar = false;
      });
    }else showBar = true;
  }
}
