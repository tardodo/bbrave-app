import 'dart:async';
//import 'src/flutter_webview_plugin.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:flutter/material.dart';
//import 'package:swipedetector/swipedetector.dart';
//import 'package:webview_flutter/webview_flutter.dart';
import 'package:google_fonts/google_fonts.dart';



//Call class with URL of the webpage


class WebViewContainer extends StatefulWidget {
  final url;
  final int index;
  WebViewContainer({Key key, this.url, this.index}): super(key:key);
  //WebViewContainer(this.url);
  @override
  //createState() => _WebViewContainerState(this.key ,this.url);
  createState() => _WebViewContainerState(key, this.url, index);
}
class _WebViewContainerState extends State<WebViewContainer> {
  //TextEditingController urlcont = TextEditingController(text: );
  var _url;
  final Key key;
  final int index;
  //final _key = UniqueKey();
  _WebViewContainerState(this.key,this._url, this.index);
  //final webview = new FlutterWebviewPlugin();
  double progVal;
  bool showBar = false;
  bool showTitle = false;
  final List<String> _titles = ["Articles"];
  final flutterWebviewPlugin = new FlutterWebviewPlugin();
  StreamSubscription<WebViewStateChanged> _onchanged;
  StreamSubscription<String> _onUrlChanged;// here we checked the url state if it loaded or start Load or abort Load
  StreamSubscription<double> _onProgChanged;
  StreamSubscription _onDestroy;


  @override
  void initState() {
    super.initState();
    flutterWebviewPlugin.close();
    //flutterWebviewPlugin.reloadUrl(_url);
    _onUrlChanged = flutterWebviewPlugin.onUrlChanged.listen((String url) {
      if (mounted && url != _url) {
        print("poop");
        setState(() {
          showBar = true;
          showTitle = true;
        });
      }
        if (mounted && url == _url) {
          print("poopydoo");
          setState(() {
            showBar = false;
            showTitle = false;
          });
      }
    });
    _onProgChanged =
        flutterWebviewPlugin.onProgressChanged.listen((double progress) {
          if (mounted) {
            setState(() {
              progVal = progress;
            });
          }
        });

    _onDestroy = flutterWebviewPlugin.onDestroy.listen((_) {
      if (mounted) {
        // Actions like show a info toast.
         print("Poop");
        Scaffold.of(context).showSnackBar(const SnackBar(content: const Text('Webview Destroyed')));
      }
    });

    _onchanged =
        flutterWebviewPlugin.onStateChanged.listen((WebViewStateChanged state) {
          // flutterWebviewPlugin.onStateChanged.listen((WebViewStateChanged state) {
          if (mounted) {
            if (state.type ==
                WebViewState.finishLoad) { // if the full website page loaded
              print("loaded...");
              flutterWebviewPlugin.evalJavascript("document.getElementsByClassName(\"mobile-header-bar\")[0].style.display='none';");
              flutterWebviewPlugin.evalJavascript("document.getElementsByClassName(\"mobile-header-space\")[0].style.display='none';");
              if(index == 1) {
                flutterWebviewPlugin.evalJavascript(
                    "document.getElementsByClassName(\"wpb_column vc_column_container vc_col-sm-4\")[0].style.display='none';");
                flutterWebviewPlugin.evalJavascript("document.getElementsByClassName(\"vc_row wpb_row vc_row-fluid\")[0].style.display='none';");
              }

              if(showTitle == true) {
                flutterWebviewPlugin.evalJavascript(
                    "document.getElementsByClassName(\"page-title-breadcrumbs\")[0].style.display='none';");
                flutterWebviewPlugin.evalJavascript("document.getElementsByClassName(\"entry-title\")[0].style='color:rgb(37, 61, 76);'");
              }else{
                flutterWebviewPlugin.evalJavascript(
                    "document.getElementsByClassName(\"page-title title-left page-title-responsive-enabled\")[0].style.display='none';");
              }
              flutterWebviewPlugin.evalJavascript("document.getElementsByClassName(\"footer solid-bg footer-outline-decoration\")[0].style.display='none';");
              //flutterWebviewPlugin.show();
              //flutterWebviewPlugin.evalJavascript("document.getElementsByClassName(\"top-bar top-bar-line-hide\")[0].style.display='none';");
              //flutterWebviewPlugin.evalJavascript("document.getElementsByClassName(\"dt-close-mobile-menu-icon\")[0].style.display='none';");
            } else if (state.type == WebViewState
                .abortLoad) { // if there is a problem with loading the url
              print("there is a problem...");
            } else if (state.type ==
                WebViewState.startLoad) { // if the url started loading
              print("start loading...");
            } else {
              //flutterWebviewPlugin.hide();
              print("Guac" + "${state.url}");
            }
          }
        }
        );



  }


  @override
  Widget build(BuildContext context) {
          return WebviewScaffold(
              appBar: showBar? AppBar(
                iconTheme: IconThemeData(color: Colors.black),
                backgroundColor: Colors.white,
                leading:IconButton(
                  icon: Icon(Icons.arrow_back, color: Colors.black),
                  onPressed:() {
                    flutterWebviewPlugin.goBack();
                  },
                ),
                title: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Image.asset(
                    'assets/bbrave_logo.jpg',
                    fit: BoxFit.cover,
                    height: 35.0,
                  ),
                ],
              ),
              ) :AppBar(
                iconTheme: IconThemeData(color: Colors.black),
                backgroundColor: Colors.white,
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Image.asset(
                      'assets/bbrave_logo.jpg',
                      fit: BoxFit.cover,
                      height: 35.0,
                    ),
                  ],
                ),
              ),
              key: key,
              url: _url,
              withJavascript: true,
              // run javascript
              withZoom: false,
              // if you want the user zoom-in and zoom-out
              hidden: true,
              clearCache: true,
              clearCookies: true,

              initialChild: Container( // but if you want to add your own waiting widget just add InitialChild
                color: Colors.white,
                child: Center(
                  child: CircularProgressIndicator(value: progVal, valueColor: new AlwaysStoppedAnimation<Color>(Color(0xFFef7d40))),
                ),)
      );
  }

  @override
  void dispose() {
    _onDestroy.cancel();
    _onchanged.cancel();
    _onUrlChanged.cancel();
    _onProgChanged.cancel();
    flutterWebviewPlugin.dispose();
    print("Booboo");
    super.dispose();
    flutterWebviewPlugin.close();
  }
}


