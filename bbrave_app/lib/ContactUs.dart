import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'webview.dart';
import 'package:fluttericon/elusive_icons.dart';
import 'package:google_fonts/google_fonts.dart';

class Contact extends StatelessWidget {
  final bCol = const Color(0xFFef7d40);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(32, 4, 32, 15),
              child: Text(
                "CONTACT US",
                style: GoogleFonts.robotoSlab(
                    fontSize: 25,
                    fontStyle: FontStyle.normal,
                    color: Color.fromRGBO(37, 61, 76, 1),
                    fontWeight: FontWeight.bold),
              ),
            ),
            Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        IconButton(
                          tooltip: "Phone",
                          iconSize: 60,
                          icon: Icon(
                            Icons.call,
                            color: bCol,
                          ),
                          onPressed: _launchurl,
                        ),
                        Text(
                          "CALL",
                          style: TextStyle(
                              fontSize: 13,
                              fontWeight: FontWeight.w400,
                              color: Color(0xFFF37F49)),
                        ),
                      ]),
                  Theme(
                    data: ThemeData(
                      accentColor: Color(0xFFef7d40),
                      textSelectionColor: Color.fromRGBO(251, 189, 83, 0.5),
                    ),
                    child: SelectableText(
                      '+356 7980 8182      ',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                ]),
            Container(
              height: 10,
            ),
            Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        IconButton(
                          tooltip: "Email",
                          iconSize: 60,
                          icon: Icon(
                            Icons.email,
                            color: bCol,
                          ),
                          onPressed: _launchurl1,
                        ),
                        Text(
                          "EMAIL",
                          style: TextStyle(
                              fontSize: 13,
                              fontWeight: FontWeight.w400,
                              color: Color(0xFFF37F49)),
                        ),
                      ]),
                  Theme(
                    data: ThemeData(
                      accentColor: Color(0xFFef7d40),
                      textSelectionColor: Color.fromRGBO(251, 189, 83, 0.5),
                    ),
                    child: SelectableText(
                      'info@bbrave.org.mt',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                ]),
            Container(
              height: 10,
            ),
            Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        IconButton(
                          tooltip: "Contact Form",
                          iconSize: 60,
                          icon: Icon(
                            Icons.chat_bubble,
                            color: bCol,
                          ),
                          onPressed: () {
                            Route route = MaterialPageRoute(
                                builder: (context) => WebViewContainer(
                                      key: UniqueKey(),
                                      url: 'https://bbrave.org.mt/contact/',
                                      index: 1,
                                    ));
                            Navigator.push(context, route);
                          },
                        ),
                        Text(
                          "FORM",
                          style: TextStyle(
                              fontSize: 13,
                              fontWeight: FontWeight.w400,
                              color: Color(0xFFF37F49)),
                        ),
                      ]),
                  Text(
                    'Contact Form           ',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ]),
            Container(
              height: 20,
            ),
            Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        IconButton(
                          tooltip: "Facebook",
                          iconSize: 40,
                          icon: Icon(
                            Elusive.facebook,
                            color: bCol,
                          ),
                          onPressed: _launchurl2,
                        ),
                        Text(
                          "FACEBOOK",
                          style: TextStyle(
                              fontSize: 13,
                              fontWeight: FontWeight.w400,
                              color: Color(0xFFF37F49)),
                        ),
                      ]),
                  Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        IconButton(
                          iconSize: 40,
                          tooltip: "LinkedIn",
                          icon: Icon(
                            Elusive.linkedin,
                            color: bCol,
                            semanticLabel: "Title",
                          ),
                          onPressed: _launchur13,
                        ),
                        Text(
                          "LINKEDIN",
                          style: TextStyle(
                              fontSize: 13,
                              fontWeight: FontWeight.w400,
                              color: Color(0xFFF37F49)),
                        ),
                      ]),
                  Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        IconButton(
                          tooltip: "Instagram",
                          iconSize: 40,
                          icon: Icon(
                            Elusive.instagram,
                            color: bCol,
                          ),
                          onPressed: _launchurl4,
                        ),
                        Text(
                          "INSTAGRAM",
                          style: TextStyle(
                              fontSize: 13,
                              fontWeight: FontWeight.w400,
                              color: Color(0xFFF37F49)),
                        ),
                      ]),
                ])
          ],
        ),
      ),
    );
  }

  void _launchurl() async {
    // ignore: dead_code
    const phonenumber = 'tel:+35679808182';
    if (await canLaunch(phonenumber)) {
      await launch(phonenumber);
    } else {
      throw 'Error';
    }
  }

  void _launchurl1() async {
    // ignore: dead_code
    const emailaddress = 'mailto:info@bbrave.org.mt';
    if (await canLaunch(emailaddress)) {
      await launch(emailaddress);
    } else {
      throw 'Error';
    }
  }

  void _launchurl2() async {
    // ignore: dead_code
    //const facebook = 'https://www.facebook.com/bbrave.org.mt/';
    const facebookApp = 'fb://page/1838789819482766';
    const facebookURL = 'fb.me/bbrave.org.mt';
    if(await canLaunch(facebookApp)){
      await launch(facebookApp);
    }else if (await canLaunch('http://$facebookURL')) {
      await launch('http://$facebookURL');
    } else {
      throw 'Error';
    }
  }

  void _launchur13() async {
    // ignore: dead_code
    //const linkedin = 'https://www.linkedin.com/signup/cold-join?session_redirect=https%3A%2F%2Fwww%2Elinkedin%2Ecom%2Fgroups%2F8618921&trk=login_reg_redirect';
    const linkedin = 'https://www.linkedin.com/groups/8618921/';
    if (await canLaunch(linkedin)) {
      await launch(linkedin);
    } else {
      throw 'Error';
    }
  }

  void _launchurl4() async {
    // ignore: dead_code
    const insta = 'https://www.instagram.com/bbrave_malta/';
    if (await canLaunch(insta)) {
      await launch(insta);
    } else {
      throw 'Error';
    }
  }
}
