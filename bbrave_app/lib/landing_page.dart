import 'package:bbrave_app/ContactUs.dart';
import 'package:flutter/material.dart';
import 'size_config.dart';
import 'package:google_fonts/google_fonts.dart';
import 'home_pg.dart';
import 'package:bbrave_app/custom_icon/need_help_icon_icons.dart';

class Landing extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      child: Scaffold(
        backgroundColor: Color.fromRGBO(25, 62, 81, 1),
        body: SafeArea(
        child:
          Container(
          height: SizeConfig.screenHeight,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(20)),
              border: Border.all(color: Color.fromRGBO(25, 62, 81, 1), width: 6)
          ),
          child:
        ListView(
          children: [
            slogan,
            Image.asset(
              'assets/bbrave_logo.jpg',
              height: SizeConfig.blockSizeVertical * 8.0,
            ),
            textSection,
            Container(
              height: SizeConfig.blockSizeVertical * 5.0,
            ),
            LandingWidget1(),
            Container(
              height: SizeConfig.blockSizeVertical * 5.0,
            ),
            LandingWidget2(),
          ],
        ),
      ),
      ),
      ),
    );
  }
}

Widget textSection = Container(
  padding: EdgeInsets.fromLTRB(32, 50,32,50),
  child: Text(
    'bBrave is the anti-bullying NGO in Malta',
    softWrap: true,
    textAlign: TextAlign.center,
    style: GoogleFonts.aleo(
      fontSize: 20,
      color: Colors.black,
    ),
  ),
);

Widget slogan = Container(
  padding:  EdgeInsets.fromLTRB(32, 45, 32,40),
  child: Text(
    '"No one heals himself by wounding another"',
    softWrap: true,
    textAlign: TextAlign.center,
    style: TextStyle(
      fontSize: 18,
      fontStyle: FontStyle.italic,
      color: Colors.black,
    ),
  ),
);

class LandingWidget1 extends StatefulWidget {
  LandingWidget1({Key key}) : super(key: key);

  @override
  _LandingWidgetState1 createState() => _LandingWidgetState1();
}

class _LandingWidgetState1 extends State<LandingWidget1> {
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.person),
              color: Color(0xFFF37F49),
              iconSize: 50,
              onPressed: () {
                Route route = MaterialPageRoute( builder: (context) =>
                    Home(index: 1,));
                Navigator.pushReplacement(context, route);
              },
            ),
            Text('JOIN BBRAVE',
              style: TextStyle(
                  fontSize: 13,
                  fontWeight: FontWeight.w400,
                  color: Color(0xFFF37F49)),
            ),
          ],
        ),
       /* Container(
          width: SizeConfig.blockSizeHorizontal * 10.0,
        ),

        */
        Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            IconButton(
              icon: Icon(NeedHelpIcon.needhelp),
              color: Color(0xFFF37F49),
              iconSize: 50,
              onPressed: () {
                Route route = MaterialPageRoute( builder: (context) =>
                    Home(index: 2,));
                Navigator.pushReplacement(context, route);
              },
            ),
            Text('I NEED HELP',
              style: TextStyle(
                  fontSize: 13,
                  fontWeight: FontWeight.w400,
                  color: Color(0xFFF37F49)),
            ),
          ],
        ),
       /* Container(
          width: SizeConfig.blockSizeHorizontal * 10.0,
        ),

        */
        Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.mail),
              color: Color(0xFFF37F49),
              iconSize: 50,
              onPressed: () {
                Route route = MaterialPageRoute( builder: (context) =>
                    Home(index: 4,));
                Navigator.pushReplacement(context, route);
              },
            ),
            Text('CONTACT US',
              style: TextStyle(
                  fontSize: 13,
                  fontWeight: FontWeight.w400,
                  color: Color(0xFFF37F49)),
            ),
          ],
        ),
      ],
    );
  }
}

class LandingWidget2 extends StatefulWidget {
  LandingWidget2({Key key}) : super(key: key);

  @override
  _LandingWidgetState2 createState() => _LandingWidgetState2();
}

class _LandingWidgetState2 extends State<LandingWidget2> {
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.library_books),
              color: Color(0xFFF37F49),
              iconSize: 50,
              onPressed: () {
                Route route = MaterialPageRoute( builder: (context) =>
                    Home(index: 0,)); //Fix to articles
                Navigator.pushReplacement(context, route);
              },
            ),
            Text('ARTICLES',
              style: TextStyle(
                  fontSize: 13,
                  fontWeight: FontWeight.w400,
                  color: Color(0xFFF37F49)),
            ),
          ],
        ),
        Container(
          width: SizeConfig.blockSizeHorizontal * 15.0,
        ),


        Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.videogame_asset),
              color: Color(0xFFF37F49),
              iconSize: 50,
              onPressed: () {
                Route route = MaterialPageRoute( builder: (context) =>
                    Home(index: 3,));
                Navigator.pushReplacement(context, route);
              },
            ),
            Text('GAME',
              style: TextStyle(
                  fontSize: 13,
                  fontWeight: FontWeight.w400,
                  color: Color(0xFFF37F49)),
            ),
          ],
        ),
      ],
    );
  }
}


