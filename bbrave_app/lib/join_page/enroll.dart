import 'package:flutter/material.dart';
import 'individual_membership.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:google_fonts/google_fonts.dart';

class Enroll extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        body: ListView(
          children: [
            titleSection,
            textSection,
            EnrollWidget(),
            instructionSection,
          ],
        ),
      ),
    );
  }
}

  Widget titleSection = Container(
    padding: EdgeInsets.fromLTRB(32, 22, 32, 5),
    child: Text(
      'JOIN BBRAVE',
      textAlign: TextAlign.center,
      style: GoogleFonts.robotoSlab(
        fontSize: 25,
        fontStyle: FontStyle.normal,
        fontWeight: FontWeight.bold,
        color: Color.fromRGBO(37, 61, 76, 1),
      ),
    ),
  );

  Widget textSection = Container(
    padding: EdgeInsets.fromLTRB(32, 10, 32, 15),
    child: Text(
      'Enrol with bBrave and support our cause. You can register for the individual membership option or if you are a business and would like to enrol one or more members, please use the corporate membership form.',
      softWrap: true,
      style: TextStyle(
        fontSize: 16,
        color: Colors.grey[500],
      ),
    ),
  );

  Widget instructionSection = Container(
    padding: EdgeInsets.fromLTRB(32, 15, 32, 32),
    child: Text(
      'Download and fill in this form to register as a company and '
          'enrol yourself and your employees in bBrave',
      textAlign: TextAlign.center,
      style: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 16,
        color: Color(0xFFF37F49),
      ),
    ),
  );

class EnrollWidget extends StatefulWidget {
  EnrollWidget({Key key}) : super(key: key);

  @override
  _EnrollWidgetState createState() => _EnrollWidgetState();
}

class _EnrollWidgetState extends State<EnrollWidget> {
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        IconButton(
          icon: Icon(Icons.account_circle),
          color: Color(0xFFF37F49),
          iconSize: 72,
          onPressed: () {
          Route route = MaterialPageRoute( builder: (context) =>
            Individual());
              Navigator.push(context, route);
              },
        ),
        Text('Individual Membership',
          style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              color: Colors.black),
        ),
        IconButton(
          icon: Icon(Icons.supervised_user_circle),
          color: Color(0xFFF37F49),
          iconSize: 72,
          onPressed: () {
            _launchURL();
          },
        ),
        Text('Corporate Membership',
          style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              color: Colors.black),
        ),
      ],

    );
  }
}


_launchURL() async {
   const url = 'https://bbrave.org.mt/wp-content/uploads/2018/11/bBrave-Membership-Form-V4-CORPORATE-14May18.pdf';
   if (await canLaunch(url)) {
     await launch(url);
   } else {
     throw 'Could not launch $url';
   }
 }
