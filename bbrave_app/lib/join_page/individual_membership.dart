import 'package:flutter/material.dart';
import '../webview.dart';
import 'package:google_fonts/google_fonts.dart';

class Individual extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
   return Container(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Image.asset(
                'assets/bbrave_logo.jpg',
                fit: BoxFit.cover,
                height: 35.0,
              ),
            ],
          ),
        ),
        body: ListView(
          children: [
            titleSection,
            IndividualWidget(),
          ],
        ),
      ),
    );
  }
}

Widget titleSection = Container(
  padding: EdgeInsets.fromLTRB(32, 22, 32, 15),
  child: Text(
    'JOIN BBRAVE',
    textAlign: TextAlign.center,
    style: GoogleFonts.robotoSlab(
      fontSize: 25,
      fontStyle: FontStyle.normal,
      fontWeight: FontWeight.bold,
      color: Color.fromRGBO(37, 61, 76, 1),
    ),
  ),
);

class IndividualWidget extends StatefulWidget {
  IndividualWidget({Key key}) : super(key: key);

  @override
  _IndividualWidgetState createState() => _IndividualWidgetState();
}

class _IndividualWidgetState extends State<IndividualWidget> {
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        IconButton(
          icon: Icon(Icons.account_circle),
          color: Color(0xFFF37F49),
          iconSize: 72,
          onPressed: () {
            Route route = MaterialPageRoute( builder: (context) =>
                WebViewContainer(key: UniqueKey(), url: 'https://bbrave.org.mt/adult-registration/'));
            Navigator.push(context, route);
          },
        ),
        Text('Adult Registration',
          style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              color: Colors.black),
        ),
        IconButton(
          icon: Icon(Icons.account_circle),
          color: Color(0xFFF37F49),
          iconSize: 72,
          onPressed: () {
            Route route = MaterialPageRoute( builder: (context) =>
                WebViewContainer(key: UniqueKey(), url: 'https://bbrave.org.mt/minor-registration/'));
            Navigator.push(context, route);
          },
        ),
        Text('Minor Registration(Under 18)',
          style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              color: Colors.black),
        ),
        IconButton(
          icon: Icon(Icons.account_circle),
          color: Color(0xFFF37F49),
          iconSize: 72,
          onPressed: () {
            Route route = MaterialPageRoute( builder: (context) =>
                WebViewContainer(key: UniqueKey(), url:'https://bbrave.org.mt/family-registration/'));
            Navigator.push(context, route);
          },
        ),
        Text('Family Registration(2-adults and 2-minors)',
         style: TextStyle(
             fontSize: 18,
             fontWeight: FontWeight.bold,
             color: Colors.black),
        ),
      ],

    );
  }
}

